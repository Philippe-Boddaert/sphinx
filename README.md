# Le Sphinx

"Le Sphinx" est un être doté de pouvoirs exceptionnel de divination.

Il demande à l'utilisateur de choisir un nombre entre 1 et 100, et le garde secret.

À chaque proposition fausse, l'utilisateur donne une indication "c'est plus" (si le nombre recherché est plus grand) ou "c'est moins" (si le nombre recherché est plus petit).

1. Le Sphinx parie avec ce passant 100€ qu'il peut déterminer ce nombre en, au plus, 7 propositions. 
    
    __Comment "Le Sphinx" procède-t-il ? (Sachant qu'il gagne toujours...)__
2. Pour impressionner encore plus le passant, Le Sphinx lui propose de choisir un nombre entre 1 et 4 milliards. 
    
    __En combien, au maximum, le Sphinx peut-il trouver le nombre choisi à coup sûr ?__
