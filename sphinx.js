let minimum = 1;
let maximum = 0;
let proposition = 0;
let coups = 0;

const panel_0 = document.querySelector("#panel_0");
const panel_1 = document.querySelector("#panel_1");
const panel_2 = document.querySelector("#panel_2");
const panel_3 = document.querySelector("#panel_3");
const panel_4 = document.querySelector("#panel_4");
const guess = document.querySelector("#proposition");
const plus = document.querySelector("#plus");
const moins = document.querySelector("#moins");

function devine(){
  coups++;
  milieu = Math.trunc((minimum + maximum) / 2);
  if (minimum == milieu){
    moins.disabled = true;
  } else {
    moins.disabled = false;
  }
  if (maximum == milieu){
    plus.disabled = true;
  } else {
    plus.disabled = false;
  }
  return milieu;
}

function trouve(){
  document.querySelector("#coups").innerHTML = coups;
  panel_3.classList.add("hidden");
  panel_4.classList.remove("hidden");
}

document.querySelector("#intervalle").addEventListener('click', function(){
    maximum = parseInt(document.querySelector("#selection").value, 10);
    document.querySelector("#borne").innerHTML = maximum;
    panel_0.classList.add("hidden");
    panel_1.classList.remove("hidden");
});

document.querySelector("#jouer").addEventListener('click', function(){
    document.querySelectorAll(".coups").forEach(function(element){
      element.innerHTML = Math.floor(Math.log2(maximum)) + 1;
    });
    panel_1.classList.add("hidden");
    panel_2.classList.remove("hidden");

    proposition = devine();
    guess.innerHTML = proposition;
});

document.querySelector("#moins").addEventListener('click', function(){
    maximum = proposition - 1;
    proposition = devine();
    guess.innerHTML = proposition;
    if (minimum == maximum){
      trouve();
    }
});

document.querySelector("#plus").addEventListener('click', function(){
    minimum = proposition + 1;
    proposition = devine();
    guess.innerHTML = proposition;
    if (minimum == maximum){
      trouve();
    }
});

document.querySelector("#trouve").addEventListener('click', trouve);
